# Destructuring

To understand destructuring, we first need to understand what is **assignment** and how it works in JavaScript. The assignment operator (`=`) assigns the value on its **right** to the value on its **left**. e.g. `let a = 42;`, here the value `42`, which is on the right side of (`=`) got assigned to variable `a`, which is on the left side of the assignment operator.

Destructuring is **unpacking** of **values from arrays** or **properties from objects** to the variable of our choice. It is a form of syntax that was introduced in ES6. Let's look at a few examples to understand it better.

**Array**
```js
let array = ["John", "Wick"];
```
**Before destructuring syntax**
```js
let firstName = array[0];
let lastName = array[1];

console.log(firstName); //-> "John"
console.log(lastName); //-> "Wick"
```
**After destructuring syntax**
```js
let [firstName, lastName] = array;

console.log(firstName); //-> "John"
console.log(lastName); //-> "Wick"
```

**Object**
```js
let obj = {
    fName: "John",
    age: 42,
    job: "Monk",
    city: "NY",
    favPet: "Dog",
}
```
**Before destructuring syntax**
```js
let fName = obj.fName;
let age = obj.age;

console.log(fName); //-> "John"
console.log(age); //-> 42
```
**After destructuring syntax**
```js
let {fName, age} = obj;

console.log(fName, age); //-> "John" 42
```

In array destructuring, if the number of variables on the left of the (`=`) are more than the number of elements on the right then the extra variables will be assigned the value of `undefined`.
```js
let [a, b, c] = [1, 2];

console.log(a, b, c); //-> 1 2 undefined
```

On the other hand, if we have less number of variables than elements in the array, we can assign the rest of the elements to a single variable using (`...`) **rest** operator.
```js
let [a, b, c, ...remaining] = [1,2,3,4,5];

console.log(a, b, c); //-> 1 2 3
console.log(remaining); //-> [4, 5]
```

We can provide default values to variables in case the value `undefined` is to be assigned to them from the array.
```js
//before assigning default value
let [a, b, c] = [1, 2];

console.log(a,b,c); //-> 1 2 undefined

//after assigning default value
let [a, b, c = 3] = [1, 2]

console.log(a, b, c); //-> 1 2 3
```

Similarly, we can do in object destructuring, we can assign default values to variables, in case the unpacked value is `undefined`.
```js
let {a, b, c = 3} = {a: 1, b: 2};

console.log(a, b, c); //-> 1 2 3
```

In objects we can also assign new variable names to the keys getting unpacked.
```js
let obj = {
    name1: "Neo",
    name2: "Trinity",
    name3: "Morpheus",
}

let {name1: char1, name2: char2, name3: char3} = obj;

console.log(char1, char2, char3); //-> Neo Trinity Morpheus
```

We can also destructure **nested** arrays and objects, following similar syntax. We need to pay attention to the level of values or properties that we want to destructure.
```js
let nestedArrObj = {
    movie: "Matrix",
    sequels: [1, 2, 3],
}

let {
    movie: film,
    sequels: [part1]
} = nestedArrObj;

console.log(film, part1); //-> Matrix 1
```

---
### References
- [MDN document on Destructuring](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment)